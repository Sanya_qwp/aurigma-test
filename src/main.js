import Vue from 'vue'
import Vuex from 'vuex'
import App from './App.vue'
import router from './router/router.js'

import 'normalize.css'
import axios from 'axios'
import VueAxios from 'vue-axios'
Vue.use(VueAxios, axios);
Vue.use(Vuex);
import store from './store/store.js'

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: { App },
  template: '<App/>'
})
