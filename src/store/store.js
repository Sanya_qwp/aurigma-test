import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);
import router from 'vue-router'
Vue.use(router);

export default new Vuex.Store({
  state: {
    isMenuOpen: false,
    windowTitle: 'Select an image',
    activeCard: '',
    urlState: ['animals'],
  },
  mutations: {
    menuStateToggle(state) {
      state.isMenuOpen = !state.isMenuOpen
    },
    windowTitleToggle(state) {
      if (state.windowTitle == 'Select an image') {
        state.windowTitle = 'Select a category';
      }
      else {
        state.windowTitle = 'Select an image';
      }
    },
    setActiveCard(state, card) {
      state.activeCard = card;
    },
    setUrl(state) {
      //str -> array
      let str = window.location.hash.split('#');
      //remove empty value str[0] = ""
      str.splice(0, 1);

      //string to array ('/' is separator)
      state.urlState = str[0].split('/');
      for (let i = 0; i < str.length - 1; i++) {
        //push to state
        state.urlState.push({'id': i, 'name': state.urlState[i]});
      }
      
    }
  },
  actions: {
    menuStateToggle({commit}) {
      commit('menuStateToggle');
      commit('windowTitleToggle');
    }
  }
});