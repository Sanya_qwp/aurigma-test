import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

import Empty from '../components/Empty.vue'
import Content from '../components/Content.vue'

export default new Router({
  mode: 'history',
  routes: [
    {
      path: '*',
      name: 'index',
      component: Content
    },
    // {
    //   path: '*',
    //   name: 'empty-folder',
    //   component: Empty
    // }
  ]
})
