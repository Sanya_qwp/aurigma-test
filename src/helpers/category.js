import store from '../store/store.js'

export function categoryToggle(event) {
  const categories = document.getElementsByClassName('category__title');
  categoryActiveRemove(categories);
  addActiveClass(event.target);
  store.commit('setUrl')
}

//removes category_active class from category__title
function categoryActiveRemove(categories) {
  //classList.remove doesnt work on categories.classlist remove but categories[i].classlist.remove is works
  for (let i = 0; i < categories.length; i++) {
    categories[i].classList.remove('category_active');
  }
}

//event.target add class category_active
function addActiveClass(target) {
  if (target.classList.contains('category__title')) {
    target.classList.add('category_active')
    doesFolderContainContent(target)
  }
}

export function mobileMenuBtnClicked() {
  menuClassToggle()
}

function menuClassToggle() {
  const windowPanel = document.getElementById('windowPanel'),
        categories = document.getElementById('categories');

  if (categories.classList.contains('categories_active')) {
    categories.classList.remove('categories_active');  
    windowPanel.classList.remove('panel_active');  
  }
  else {
    categories.classList.add('categories_active');
    windowPanel.classList.add('panel_active');
  }
}