export function cardClicked(event) {
  const cards = document.getElementsByClassName('card');
  for (let i = 0; i < cards.length; i++) {
    cards[i].classList.remove('card_active');
  }
  event.currentTarget.classList.add('card_active');
}